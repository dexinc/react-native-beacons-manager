package com.mackentoch.beaconsandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import com.mackentoch.beaconsandroid.BeaconsAndroidTransitionService;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.bluetooth.le.ScanFilter;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.Build;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import java.util.List;
import com.facebook.react.HeadlessJsTaskService;

import static com.mackentoch.beaconsandroid.BeaconsAndroidModule.LOG_TAG;

public class BeaconsAndroidBootReceiver extends BroadcastReceiver
{
  @Override
  public void onReceive(Context context, Intent intent)
  {
    Log.i(LOG_TAG, "BeaconsAndroidBootReceiver onReceive...");

    Log.i(LOG_TAG, "intent: " + intent.getAction());

    //context.startService(new Intent(context, BeaconsAndroidTransitionService.class));

    //start the background process

    if (!isAppOnForeground(context)) {
      Log.d(LOG_TAG, "BeaconsAndroidBootReceiver start... !isAppOnForeground");

//      Intent myIntent = new Intent(context, BeaconsAndroidTransitionService.class);
//      myIntent.putExtra("defaultTimeout", 2000);
//
//      context.startService(myIntent);
//    Log.d(LOG_TAG, "BeaconsAndroidBootReceiver start... startService");
////      getApplicationContext().startService(myIntent);
//      HeadlessJsTaskService.acquireWakeLockNow(context);
//    Log.d(LOG_TAG, "BeaconsAndroidBootReceiver start... acquireWakeLockNow");


      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Log.d(LOG_TAG, "BeaconsAndroidBootReceiver startForegroundService...");

        context.startForegroundService(new Intent(context, BeaconsAndroidTransitionService.class));
      } else {
        Log.d(LOG_TAG, "BeaconsAndroidBootReceiver startService...");

        context.startService(new Intent(context, BeaconsAndroidTransitionService.class));
      }
      HeadlessJsTaskService.acquireWakeLockNow(context);

    }else {
      Log.d(LOG_TAG, "BeaconsAndroidBootReceiver onReceive...isAppOnForeground...do nothing ");
    }
//
//
//      //run the service on boot (So app does not need to be opened)
////    HeadlessJsTaskService.acquireWakeLockNow();
//
//      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//        Log.d(LOG_TAG, "BeaconsAndroidBootReceiver startForegroundService...");
//
//        context.startForegroundService(new Intent(context, BeaconsAndroidTransitionService.class));
//      } else {
//        Log.d(LOG_TAG, "BeaconsAndroidBootReceiver startService...");
//
//        context.startService(new Intent(context, BeaconsAndroidTransitionService.class));
//      }
//      HeadlessJsTaskService.acquireWakeLockNow(context);
//
//
//    }
  }

  private boolean isAppOnForeground(Context context) {
    /**
     We need to check if app is in foreground otherwise the app will crash.
     http://stackoverflow.com/questions/8489993/check-android-application-is-in-foreground-or-not
     **/
    ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningAppProcessInfo> appProcesses =
      activityManager.getRunningAppProcesses();
    if (appProcesses == null) {
      return false;
    }
    final String packageName = context.getPackageName();
    for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
      if (appProcess.importance ==
        ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
        appProcess.processName.equals(packageName)) {
        return true;
      }
    }
    return false;
  }


}
